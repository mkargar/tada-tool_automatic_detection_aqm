#include "tada.h"
#include <stdbool.h>
#include <sys/ioctl.h>

#define TIME_WAIT 1000
int client_shutdown = 0;

struct cmd_args cmd_args;

time_t test_time;
int session_complete = 0;
int unexpected_exit = 0;
int udpthread_exit = 1;
int testNumber = 0;
float lastLossRate = 0.0;

int udp_packetCount = 0;
int udp_packetSize = 0;
double tDistributionTable[TTABLE_ROW_NUMBER][TTABLE_COLUMN_NUMBER];

int channelError = 0;
int *channelErrorArray = 0;
int channelErrorArraySize = 0;

pthread_mutex_t session_complete_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t unexpected_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t udpthread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t udpthread_exit_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t signal_set_mutex = PTHREAD_MUTEX_INITIALIZER;
int signal_set = 0;

char fileNamePrefix[100] = "empty";       /* prefix of result files names  */
char resultFolderName[100] = "./results";
char queueTimeFileName[500] = "empty";  
char infoFileName[500] = "empty";    
char lossDensityFileName[500] = "empty";      
char lossNumberFileName[500] = "empty";      
char queueDerivationFileName[500] = "empty";
char pctPdtMedianFileName[500] = "empty";
char queueTypeFileName[500] = "empty";
char currentTime[80];

int sending = 0; // 1 of currently sending, else 0
int receiving = 1; // 1 of currently receiving, else 0

/* Initialize Control Channel */
int initializeControlConn(struct cmd_args *cmd_args) {

	int i, len, rc, on = 1;
    int listen_sd, max_sd, new_sd;
    int desc_ready, end_server = FALSE;
    int close_conn;
    int status = 1;
    char buffer[80];
    struct timeval timeout;
    fd_set master_set, working_set;

    struct connectionInfo *conn = &cmd_args->controlConn;

	/* Open /dev/null to dispose of data */
	if ((conn->dev_null = fopen("/dev/null", "a")) == NULL) {
		printf("   Could not open /dev/null\n");
		return set_error_signal();
	}

	/* Create the TCP socket */
	listen_sd = conn->listensock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (conn->listensock < 0) {
		perror("   Failed to create TCP socket\n");
		return set_error_signal();
	}

	struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) {
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(conn->listensock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}

	}*/

    

	/* Construct the TCP server sockaddr_in structure */
	memset(&(conn->si_me), 0, sizeof(struct sockaddr_in));         /* Clear struct */
	conn->si_me.sin_family = AF_INET;                 /* Internet/IP */
	conn->si_me.sin_addr.s_addr = htonl(INADDR_ANY);  /* Incoming addr */
	conn->si_me.sin_port = htons(conn->server_port);  /* server port */

	/* Bind the TCP server socket */
	if (bind(conn->listensock,
		 (struct sockaddr *) &(conn->si_me), sizeof(struct sockaddr_in)) < 0) {
		perror("Failed to bind the TCP server socket\n");
		return set_error_signal();
	}

	/* Listen on the server socket */
	if (listen(conn->listensock, MAXPENDING) < 0) {
		perror("Failed to listen on server socket\n");
		return set_error_signal();
	}

	status = handle_new_controlConn(conn,0);
	if (status >= 0) {
		cmd_args->glblPars.tada_phase = RATE_DETECTION_PHASE;
		printf("\n----------------------------------------\n\n");
		printf("\n   New Control Connection!\n");
		printf("----------------------------------------\n\n");
	} 
		

	// Set starting timestamp
	gettimeofday(&conn->start_time, NULL);	

	return 1;
}

int initializeUDPconn(struct cmd_args *cmd_args) {

	pthread_mutex_lock(&unexpected_exit_mutex);
		if (unexpected_exit == 1) {
			pthread_mutex_unlock(&unexpected_exit_mutex);
			return -1;
		}
	pthread_mutex_unlock(&unexpected_exit_mutex);

	struct connectionInfo *conn = &cmd_args->udpConn;
	/* Create the UDP socket*/
	conn->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (conn->sock < 0) {
		printf("   Failed to create UDP socket");
		return -1;
	}

	struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(conn->sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}
	}*/

    

	/* Construct the TCP server sockaddr_in structure */
	memset(&(conn->si_me), 0, sizeof(struct sockaddr_in));         /* Clear struct */
	conn->si_me.sin_family = AF_INET;                 /* Internet/IP */
	conn->si_me.sin_addr.s_addr = htonl(INADDR_ANY);  /* Incoming addr */
	conn->si_me.sin_port = htons(conn->server_port);          /* server port */

	//Make UDP server non-blocking.
	int flags = fcntl(conn->sock, F_GETFL, 0);
	if (flags == -1) {
		printf("   fcntl\n");
		return -1;
	}

	if (fcntl(conn->sock, F_SETFL, O_NONBLOCK | flags) == -1) {
		printf("   fcntl\n");
		return -1;
	}

	/* Bind the UDP server socket */
	if (bind(conn->sock,
		 (struct sockaddr *) &(conn->si_me), sizeof(struct sockaddr_in)) < 0) {
		perror("   Failed to bind the UDP server socket\n");
		return -1;
	}

	// Set starting timestamp
	gettimeofday(&conn->start_time, NULL);
	return 1;
}

int resetRCV(struct cmd_args *cmd_args) {
	struct connectionInfo *udpConn_ci = &cmd_args->udpConn;
	struct connectionInfo *controlConn_ci = &cmd_args->controlConn;
	int rc, on = 1;

	memset(&cmd_args->glblPars, 0, sizeof(struct globalParameters));

	rc = fd_set_blocking(udpConn_ci->sock, 1);

	// Set UDP socket to be non-blocking.
	if (rc < 0)
	{
		perror("Error Setting UDP socket Blocking");
		return -1;
	}

	cmd_args->udpConn.strInfo.transmission_duration = cmd_args->sndSideTransmission_duration;

	return 1;	

}

void signal_handler(int sig) {

	if (sig > 0) {

		char signal_str[20] = {""};

		switch (sig) {
		case SIGINT:
			strcpy(signal_str, "SIGINT");
			break;
		case SIGTERM:
			strcpy(signal_str, "SIGTERM");
			break;
		default:
			strcpy(signal_str, "Unkown");
		}

		printf("\n   Caught signal %s\n", signal_str);

		pthread_mutex_lock(&unexpected_exit_mutex);
			unexpected_exit = 1;
		pthread_mutex_unlock(&unexpected_exit_mutex);

		return;
	}
}

int main(int argc, char *argv[]) {

	// Set up signal handler
	struct sigaction new_action;
	
	/* Set up the structure to specify the new action. */
	new_action.sa_handler = &signal_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGHUP, &new_action, NULL);

	int status = 0;

	memset(&cmd_args, 0, sizeof(struct cmd_args));
	memset(&cmd_args.udpConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.controlConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.glblPars, 0, sizeof(struct globalParameters));

	cmd_args.glblPars.isReceiver = 1;

	parse_cmd_args(argc, argv, &cmd_args);

	tDistributionTable[0][0] = 0.0005;
	tDistributionTable[0][1] = 0.001;
	tDistributionTable[0][2] = 0.005;
	tDistributionTable[0][3] = 0.01;
	tDistributionTable[0][4] = 0.02;
	tDistributionTable[0][5] = 0.025;
	tDistributionTable[0][6] = 0.05;
	tDistributionTable[0][7] = 0.1;

	initializeControlConn(&cmd_args);

	//Initialize UDP server

	initializeUDPconn(&cmd_args);

	startRcvSideControlConn(&cmd_args);	
		
	pthread_mutex_lock(&udpthread_exit_mutex);
		while (udpthread_exit == 0)
			pthread_cond_wait(&udpthread_exit_cond, &udpthread_exit_mutex);
	pthread_mutex_unlock(&udpthread_exit_mutex);

	char filename[80];

	FILE *queueTypeFile;
			
	/*Opening Queue Type Result file*/
	strcpy(filename, qType_FILENAME);
	create_resultfile_name(filename, queueTypeFileName, &cmd_args.udpConn.strInfo, 1);

    if ((queueTypeFile = fopen(queueTypeFileName, "w")) <= 0){
    	printf("   Could not Create Queue Type Output file!!!\n");
    	status = -1;
    }

    fprintf(queueTypeFile, "%d\n", cmd_args.glblPars.currQueueType);

    fclose(queueTypeFile);

	if (cmd_args.updownOption == 1) {
		receiving = 0;
		sending = 1;
		printf("\n   Starting DownStream Test!!\n\n");

		status = resetRCV(&cmd_args);

		if (status > 0)
			startSndSideControlCon(&cmd_args);
	}

	

	close_program(&cmd_args);
		
}