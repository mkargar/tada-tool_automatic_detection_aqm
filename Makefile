all: tada_snd tada_rcv 

tada_rcv: tada_rcv.o tada_rcv_parse_opts.o tada_rcv_controlCH.o tada_rcv_udpStream.o tada_snd_controlCH.o tada_snd_udpStream.o tada_rcv_analyzeResults.o commonFunc.o
	gcc -ggdb -o tada_rcv tada_rcv.o tada_rcv_parse_opts.o tada_rcv_controlCH.o tada_rcv_udpStream.o tada_snd_controlCH.o tada_snd_udpStream.o tada_rcv_analyzeResults.o commonFunc.o -lpthread -lm

tada_rcv.o: tada_rcv.c
	gcc -c -g tada_rcv.c

tada_rcv_udpStream.o: tada_rcv_udpStream.c
	gcc -c -g tada_rcv_udpStream.c

tada_rcv_analyzeResults.o: tada_rcv_analyzeResults.c
	gcc -c -g tada_rcv_analyzeResults.c

tada_rcv_controlCH.o: tada_rcv_controlCH.c
	gcc -c -g tada_rcv_controlCH.c

tada_rcv_parse_opts.o: tada_rcv_parse_opts.c
	gcc -c -g tada_rcv_parse_opts.c

tada_snd: tada_snd.o tada_snd_parse_opts.o tada_snd_controlCH.o tada_snd_udpStream.o tada_rcv_controlCH.o tada_rcv_udpStream.o tada_rcv_analyzeResults.o commonFunc.o
	gcc -Wall -o tada_snd tada_snd.o tada_snd_parse_opts.o tada_snd_controlCH.o tada_snd_udpStream.o tada_rcv_controlCH.o tada_rcv_udpStream.o tada_rcv_analyzeResults.o commonFunc.o -lm -lpthread

tada_snd.o: tada_snd.c
	gcc -c -g tada_snd.c

tada_snd_parse_opts.o: tada_snd_parse_opts.c
	gcc -c -g tada_snd_parse_opts.c

tada_snd_udpStream.o: tada_snd_udpStream.c
	gcc -c -g tada_snd_udpStream.c

tada_snd_controlCH.o: tada_snd_controlCH.c
	gcc -c -g tada_snd_controlCH.c

commonFunc.o: commonFunc.c
	gcc -c -g commonFunc.c
	
clean:
	rm -f *.o
	rm -f tada_snd
	rm -f tada_rcv
