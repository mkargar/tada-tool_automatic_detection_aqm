#include "tada.h"
#include <float.h>

int udp_fd_valid(int fd) {
	if (fcntl(fd, F_GETFL) == -1 && errno == EBADF)
		return 0;
	return 1;
}

void getNextPckSizeITT(struct connectionInfo *connInfo, int *phase) {
	
}

int getUdpStreamSpecifications(struct connectionInfo *connInfo, int *phase, 
	int *sendRate, int *isFirstTest, float *lastLossRate) {
	int itt = 0;

	int localSendRate = *sendRate;
	//printf("Last Loss Rate3 is %lf\n", *lastLossRate);

	if (*phase == PACKETB_BYTEB_PHASE) {
		*isFirstTest = 0;

		connInfo->strInfo.sentPckITT_micros = (int)(connInfo->strInfo.sentPckITT_micros / 5);
		itt = connInfo->strInfo.sentPckITT_micros;
		connInfo->strInfo.sentPckSize_byte = (int)(itt * localSendRate / 1000000);
		if (connInfo->strInfo.sentPckSize_byte < MIN_PACKETSIZE_DEFAULT) {
			connInfo->strInfo.sentPckSize_byte = MIN_PACKETSIZE_DEFAULT;
			connInfo->strInfo.sentPckITT_micros = (int)(connInfo->strInfo.sentPckSize_byte * 1000000 / localSendRate);
		}
					 
	}
	else if (*phase == AQM_VS_TD_PHASE) {
		if (connInfo->strInfo.sentPckITT_micros != 0) {
			localSendRate+=(int)(localSendRate * DEFAULT_RATEADJUSTMENT_FACTOR);
			//if (*lastLossRate <= 0.0)
				//localSendRate+=(int)(localSendRate * 0.2);
			//else
				//localSendRate+=(int)(localSendRate * DEFAULT_RATEADJUSTMENT_FACTOR);
			*isFirstTest = 0;

		}
		else {
			//localSendRate+=(int)(localSendRate * 0.2);
			*isFirstTest = 1;
		}


		itt = (int)(connInfo->strInfo.sentPckSize_byte * 1000000 / localSendRate);
		if (itt > 3000) {
			itt = 3000;
			connInfo->strInfo.sentPckSize_byte = (int)(itt * localSendRate / 1000000);
			if (connInfo->strInfo.sentPckSize_byte < MIN_PACKETSIZE_DEFAULT) {
				connInfo->strInfo.sentPckSize_byte = MIN_PACKETSIZE_DEFAULT;
				itt = (int)(connInfo->strInfo.sentPckSize_byte * 1000000 / localSendRate);
			}

			if (itt > 3000)
				return ERROR;


		}

		connInfo->strInfo.sentPckITT_micros = itt;

		if (connInfo->strInfo.transmission_duration != 0) {
			connInfo->strInfo.sentPckCount = (connInfo->strInfo.transmission_duration * 1000000) / connInfo->strInfo.sentPckITT_micros;
			printf("The Numebr Of Packets to Send %d\n", connInfo->strInfo.sentPckCount);
		
		}
		else {
			connInfo->strInfo.sentPckCount = udp_packetCount;
		}// Assign the numebr of packets and itt and packet size for this phase first
		// Assign the numebr of packets and itt and packet size for this phase first
		connInfo->strInfo.sentPckSize_byte = udp_packetSize;
	}
	else if (*phase == RATE_DETECTION_PHASE) {
		udp_packetCount = connInfo->strInfo.sentPckCount;
		udp_packetSize = connInfo->strInfo.sentPckSize_byte;
		connInfo->strInfo.sentPckCount = RATEDETECTION_PACKET_COUNT;
		connInfo->strInfo.sentPckITT_micros = 100;
		connInfo->strInfo.sentPckSize_byte = MAX_PACKETSIZE_DEFAULT;
	}

	*sendRate = localSendRate;

	return 1;
}

int send_udpStream(struct connectionInfo *connInfo, int phase, int isFirstTest, int *currITT) {

	int res;
	
	int status = 1;
		
	connInfo->strInfo.bytes_sent = 0;
	connInfo->strInfo.packets_sent = 0;
	connInfo->strInfo.lastSentSeqNr = 0;

	*currITT = connInfo->strInfo.sentPckITT_micros;

	status = write_udp_data(connInfo, phase, isFirstTest);

	
	if (connInfo->strInfo.bytes_sent == 0) {
		printf("Haven't sent any data!\n");
	}


	deplete_sendbuffer(connInfo);

	return status;

}

int write_udp_data(struct connectionInfo *ci, int phase, int isFirstTest) {

	// parameters
	int send_size = 0;
	send_size = ci->strInfo.sentPckSize_byte;

	int sleep_time_ms;
	struct timeval endT, sendTime, itt, sendTime_t;
	itt.tv_sec = 0;
	itt.tv_usec = ci->strInfo.sentPckITT_micros;
	struct timeval startT, elapsedTime;
	
	time_t end_time = 0;
	int packet_count = 0;
	time_t now;

	packet_count = ci->strInfo.sentPckCount;
	

	/* Initialize send buffer */
	char buffer[MAX_PACKETSIZE_DEFAULT];
	memset(buffer, 0, MAX_PACKETSIZE_DEFAULT);

	/* Sending Start Time for this Test*/
	gettimeofday(&sendTime, NULL);

	double prPck_sTime = 0.0;
	double curPck_sTime = 0.0;
	

	memset(&ci->strInfo.session_startT, 0, sizeof(struct timeval));
	gettimeofday(&ci->strInfo.session_startT, NULL);


	while (1) {

		if (ci->strInfo.packets_sent == packet_count) {
			memset(&ci->strInfo.session_endT, 0, sizeof(struct timeval));
			gettimeofday(&ci->strInfo.session_endT, NULL);
			return 1;
		}
		

		// Ok, lets send a packet

		int rc;
		// Sending zeroes
		
		gettimeofday(&startT, NULL);

		rc = send_data(ci, buffer, send_size, prPck_sTime, &curPck_sTime, phase, isFirstTest);
		
		if (rc == -1) { // -1 means Error
			perror("UDP sendto() failed!!!");
			return ERROR;

		}
		else { // -2 means Channel Error packet, more than zero means bytes sent
			ci->strInfo.bytes_sent += send_size;
			ci->strInfo.packets_sent += 1;
			ci->strInfo.lastSentSeqNr += 1;
			prPck_sTime = curPck_sTime;
			curPck_sTime = 0.0;
		}

		timeradd(&sendTime, &itt, &sendTime_t);
		sendTime = sendTime_t;

		gettimeofday(&endT, NULL);

		if (timercmp(&endT, &sendTime, <) > 0) {
			timersub(&sendTime, &endT, &elapsedTime);
			select(0, NULL, NULL, NULL, &elapsedTime);

		}
	}

}

int send_data(struct connectionInfo *ci, char *data, uint32_t size, double prPck_sTime, 
	double *sTime, int phase, int isFirstTest) {

	struct timeval now;

	if (size < MIN_PACKETSIZE_DEFAULT) {
		fprintf(stderr, "Size of data (%d) cannot be less than MIN_PACKETSIZE (%d)", size, MIN_PACKETSIZE_DEFAULT);
		return -1;
	}

	if (channelError == 1 && phase != RATE_DETECTION_PHASE
		&& isFirstTest == 0
		&& ci->strInfo.lastSentSeqNr < channelErrorArraySize) {
		if (channelErrorArray[ci->strInfo.lastSentSeqNr] == 1) {
			return -2; // Didn't send any data
		}
	}

	char *buf = data;
	// seqNr is stored as string
	snprintf(&buf[0], 11, "%d", ci->strInfo.lastSentSeqNr);
	
	//Packet Send Time is Stored as string
	gettimeofday(&now, NULL);
	tv2s(&now, sTime);
	snprintf(&buf[11], 41, "%lf", *sTime);
	snprintf(&buf[52], 41, "%lf", prPck_sTime);


	int sent;
	if (send_all(ci, buf, size, &sent, 0) < 0) {
		return ERROR;
	}

	return sent;

}

int send_all(struct connectionInfo *ci, char *buf, int len, int *sent, int flags) {

	int total = 0;        // how many bytes we've sent
	int bytesleft = len; // how many we have left to send
	int n;

	//printf("Sending packet to %s:%d\n", inet_ntoa(ci->si_other.sin_addr), ntohs(ci->si_other.sin_port));

	while (total < len) {
		n = sendto(ci->sock, buf + total, bytesleft, flags,
                      (struct sockaddr*)&ci->si_other, sizeof(ci->si_other));

		if (n == -1) {
			break;
		}
		total += n;
		bytesleft -= n;
	}
	*sent = total; // return number actually sent here

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success

}