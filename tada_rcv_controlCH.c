#include "tada.h"

int startRcvSideControlConn(struct cmd_args *cmd_args) 
{

	pthread_mutex_lock(&unexpected_exit_mutex);
		if (unexpected_exit == 1) {
			pthread_mutex_unlock(&unexpected_exit_mutex);
			return -1;
		}
	pthread_mutex_unlock(&unexpected_exit_mutex);

	int i, len, rc, on = 1;
    int listen_sd, max_sd, accept_sd;
    int desc_ready, end_server = FALSE;
    int close_conn;
    char buffer[80];
    struct timeval timeout;
    fd_set master_set, working_set;

    int status = 1;

    struct connectionInfo *conn = &cmd_args->controlConn;
    accept_sd = conn->sock;

    //Allow socket descriptor to be reuseable
	rc = setsockopt(accept_sd, SOL_SOCKET,  SO_REUSEADDR,
	                (char *)&on, sizeof(on));
	if (rc < 0)
	{
		perror("   setsockopt() failed");
		return -1;
	}

	// Set socket to be non-blocking.

	rc = ioctl(accept_sd, FIONBIO, (char *)&on);
	if (rc < 0)
	{
		perror("   ioctl() failed");
		return -1;
	}

    /*************************************************************/
    /* Initialize the master fd_set                              */
    /*************************************************************/
    FD_ZERO(&master_set);
    max_sd = accept_sd;
    FD_SET(accept_sd, &master_set);

    /*************************************************************/
    /* Initialize the timeval struct to 1 second.  If no        */
    /* activity after 10 minutes this program will end.          */
    /*************************************************************/
    timeout.tv_sec  = 1;
    timeout.tv_usec = 0;

    do {
    	pthread_mutex_lock(&unexpected_exit_mutex);
			if (unexpected_exit == 1) {
				pthread_mutex_unlock(&unexpected_exit_mutex);
				return -1;
			}
		pthread_mutex_unlock(&unexpected_exit_mutex);
		
    	memcpy(&working_set, &master_set, sizeof(master_set));

    	rc = select(max_sd + 1, &working_set, NULL, NULL, &timeout);
    	if (rc < 0)
      	{
      		if (errno != EINTR)
        		perror("  select() failed");
        	status = -1;
        	break;
      	}

      	desc_ready = rc;
    	for (i=0; i <= max_sd  &&  desc_ready > 0; ++i)
      	{
      		if (FD_ISSET(i, &working_set)) 
      		{
      			desc_ready -= 1;
      			/****************************************************/
            	/* Check to see if this is the listening socket     */
            	/****************************************************/
            	if (i == accept_sd)
            	{
            		close_conn = FALSE;
            		int read = handle_new_controlMessage(conn->sock, cmd_args);
					if (read == 2) {
						printf("   Test Is Finished!!!\n\n");
						if (cmd_args->glblPars.isReceiver == 0
							&& cmd_args->updownOption == 1) {
							end_server = TRUE;
						}
						else 
							printf("   Waiting for Client to Shut Down!!!\n\n");

					}
					else if (read < 0) {
						if (cmd_args->glblPars.isReceiver == 0) {
							printf("   Error: Closing Program!!!\n\n");
							end_server = TRUE;
							status = -1;
						}
						else
							printf("   Error: Waiting for Client to Shut Down!!!\n\n");
					}
					else if (read == 0)	{ // Client Shut Down
						printf("\n\n   Client Shut Down: Closing Program....\n\n");
						pthread_mutex_lock(&unexpected_exit_mutex);
							unexpected_exit = 1;
						pthread_mutex_unlock(&unexpected_exit_mutex);
						status = -1;
						end_server = TRUE;
					}

            	}
      		}
      	}

    } while (end_server == FALSE);


    return status;



}


/* Handle A New Control Connection */
int handle_new_controlConn(struct connectionInfo *conn, int quickack) {

	test_time = time(NULL);

	time_to_string(currentTime);

	socklen_t addr_size = sizeof(struct sockaddr_in);
	int connection = accept(conn->listensock, (struct sockaddr *) &conn->si_other, &addr_size);

	if (connection < 0) {
		if(errno != EWOULDBLOCK) {
			perror("   accept() failed");
			return -1;
		}
		return 0;
	}

	conn->sock = connection;

	int flags = fcntl(connection, F_GETFL, 0);
	if (flags == -1) {
		printf("   Failed to get socket flags!\n");
		perror("   fcntl");
		return -1;
	}

	// Set non-blocking
	if (fcntl(connection, F_SETFL, O_NONBLOCK | flags) == -1) {
		printf("   Failed to get socket flags!\n");
		perror("   fcntl");
		return -1;
	}

	if (quickack) {
		/* Disable delayed ACKs */
		int flag = 1;
		int result = setsockopt(connection,
					IPPROTO_TCP,
					TCP_NODELAY,
					(char *) &flag,
					sizeof(int));
		if (result) {
			printf("   Failed to set TCP option TCP_QUICKACK\n");
			perror("   setsockopt");
			return -1;
		}
	}	
			
	return 1;


}

int handle_new_controlMessage(int sock, struct cmd_args *cmd_args){

	int received_number = 0;
	int packetCount = 0, packetSize = 0, packetITT = 0;
	pthread_t udpThread;
	int calculated_rate = 0;
	int res = 0;
	double lossRate = 0.0;
	struct timeval wait_time;

	wait_time.tv_sec = MIN_WAIT_TIME;
	wait_time.tv_usec = 0;

	if (cmd_args->glblPars.test_stage == GETINITIALMESSAGE_STAGE) {
		testNumber+=1;
		pthread_mutex_lock(&session_complete_mutex);
			session_complete = 0;
		pthread_mutex_unlock(&session_complete_mutex);

		int res = recv(sock, &received_number, sizeof(received_number),0);
    	if (res == 0) {
    		return 0;
    	}

    	printf("------------------------------------------------------\n");
    
    	if (res < 0) {
       		perror("   recvDecision: error getting Packet Number Message");
        	return set_error_signal();
    	}

    	received_number = ntohl(received_number);
    	packetCount = received_number;

		received_number = receiveMessage(sock);
		if (received_number >= 0)
			packetITT = received_number;
		else if (received_number == -2) { // Client Shut Down
			return 0;
		}
		else {
			printf("   Error Receiving Packet ITT Message!!!\n\n");
			return set_error_signal();

		}

		received_number = receiveMessage(sock);
		if (received_number >= 0)
			packetSize = received_number;
		else if (received_number == -2) { // Client Shut Down
			return 0;
		}
		else {
			printf("   Error Receiving Packet Size Message!!!\n\n");
			return set_error_signal();
		}

		printf("\n   Starting a Session for %d packets, %d ITT, %d packetSize\n", 
			packetCount, packetITT, packetSize);

		cmd_args->glblPars.sessionNr += 1;

		initializeUdpStream(&cmd_args->udpConn.strInfo);

		updateUdpStream(&cmd_args->udpConn.strInfo, packetCount, packetSize, packetITT);

		/* Create UDP thread */
		create_thread(&udpThread, &handle_udpThread, cmd_args);

	}
	else if (cmd_args->glblPars.test_stage == GETFINISHMESSAGE_STAGE) {
		/* Receive the message containing the test duration */
		int transmission_duration = 0;

		int res = recv(sock, &received_number, sizeof(received_number),0);
    	if (res == 0) {
    		return 0;
    	}
    
    	if (res < 0) {
       		perror("   recvDecision: error getting receiver Message");
        	return set_error_signal();
    	}

    	received_number = ntohl(received_number);
		transmission_duration = received_number;
		//printf("   Finish Message Is Received; Test Duration is: %d MicroS\n", transmission_duration);
		printf("\n   Finish Message Is Received\n");
		select(0, NULL, NULL, NULL, &wait_time);


		pthread_mutex_lock(&session_complete_mutex);
			session_complete = 1;
		pthread_mutex_unlock(&session_complete_mutex);

		pthread_mutex_lock(&udpthread_exit_mutex);
			while (udpthread_exit == 0)
				pthread_cond_wait(&udpthread_exit_cond, &udpthread_exit_mutex);
		pthread_mutex_unlock(&udpthread_exit_mutex);

		cmd_args->udpConn.strInfo.transmission_duration = transmission_duration;
		int numberOfSentPck = cmd_args->udpConn.strInfo.rcvPckCount;
		int numberOfLostPck = numberOfSentPck - cmd_args->udpConn.strInfo.pckCountReceived;
		int message = -1;
		lossRate = (numberOfLostPck * 100 / numberOfSentPck);


		if (lossRate < 100)
			res = analyzeResults(cmd_args, lossRate);
		else
			return set_error_signal();
		

		if (cmd_args->glblPars.tada_phase == PACKETB_BYTEB_PHASE) {
			
			message = htonl(res);
			if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        		perror("   Send the Finish message to SND failed!!!");
        		return set_error_signal();
    		}
    		printf("   MTU LossRate is: %f; Small LossRate is %f\n", lossRate, cmd_args->glblPars.lastLossRate);
    		return 2;		

		}
		else if (cmd_args->glblPars.tada_phase == AQM_VS_TD_PHASE) {

			switch (res) {
				case QUEUETYPE_TD:
					cmd_args->glblPars.currQueueType = QUEUETYPE_TD;
					//cmd_args->glblPars.tada_phase = PACKETB_BYTEB_PHASE;
					printf("\n      Queue Type: Tail-Drop\n");					
					cmd_args->glblPars.lastLossRate = lossRate;
					cmd_args->glblPars.tdDetectedCount += 1;
					break;

				case QUEUETYPE_ARED:
					cmd_args->glblPars.currQueueType = QUEUETYPE_ARED;
					printf("\n      Queue Type: ARED\n");
					cmd_args->glblPars.aqmDetectedCount += 1;
					cmd_args->glblPars.aredDetectedCount += 1;
					break;

				case QUEUETYPE_CODEL:
						printf("\n      Queue Type: CODEL\n");
						cmd_args->glblPars.currQueueType = QUEUETYPE_CODEL;
						cmd_args->glblPars.aqmDetectedCount += 1;
						cmd_args->glblPars.codelDetectedCount += 1;
					break;

				case QUEUETYPE_PIE:
						printf("\n      Queue Type: PIE\n");						
						cmd_args->glblPars.currQueueType = QUEUETYPE_PIE;
						cmd_args->glblPars.aqmDetectedCount += 1;
						cmd_args->glblPars.pieDetectedCount += 1;
					break;

				case QUEUETYPE_AQM:
						printf("\n      Queue Type: AQM, Not Able to Detect the Type\n");
						if (cmd_args->glblPars.currQueueType == QUEUETYPE_UNKNOWN ||
							cmd_args->glblPars.currQueueType == QUEUETYPE_TD)
							cmd_args->glblPars.currQueueType = QUEUETYPE_AQM;
						cmd_args->glblPars.aqmDetectedCount += 1;


				case QUEUETYPE_UNKNOWN:
					if (cmd_args->glblPars.currQueueType == -1)
						cmd_args->glblPars.currQueueType = QUEUETYPE_UNKNOWN;

			}			

    		/* Send this test Loss Rate */
    		message = htonl(lossRate);
    		if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        		perror("   Send the lossRate message to SND failed!!!");
        		return set_error_signal();
    		}
    		if (cmd_args->glblPars.isDefintlyTD == 1 
    			&& lossRate < DEFAULT_STOP_LOSSRATE) {

    			cmd_args->glblPars.currQueueType = QUEUETYPE_TD;
    			cmd_args->glblPars.tada_phase = PACKETB_BYTEB_PHASE;
    			/* Sending the QueueType First */
				message = htonl(QUEUETYPE_TD);
				if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        				perror("   Send the QueueType message to SND failed!!!");
        				return set_error_signal();
    			}

    		} 
    		else  {
    			if (lossRate >= DEFAULT_STOP_LOSSRATE) {
    				if (cmd_args->glblPars.currQueueType != QUEUETYPE_UNKNOWN) {
    					if (cmd_args->glblPars.sessionNr == 2)
							cmd_args->glblPars.currQueueType = QUEUETYPE_TD;
						else if (cmd_args->glblPars.aqmDetectedCount > cmd_args->glblPars.tdDetectedCount) {
							if (cmd_args->glblPars.codelDetectedCount != 0) {
								if (cmd_args->glblPars.codelDetectedCount >= cmd_args->glblPars.pieDetectedCount)
									cmd_args->glblPars.currQueueType = QUEUETYPE_CODEL;
								else if (cmd_args->glblPars.pieDetectedCount >= cmd_args->glblPars.aredDetectedCount)
									cmd_args->glblPars.currQueueType = QUEUETYPE_PIE;
								else if (cmd_args->glblPars.aredDetectedCount != 0)
									cmd_args->glblPars.currQueueType = QUEUETYPE_ARED;
							}
							else if (cmd_args->glblPars.pieDetectedCount != 0 &&
								cmd_args->glblPars.pieDetectedCount > cmd_args->glblPars.aredDetectedCount)
								cmd_args->glblPars.currQueueType = QUEUETYPE_PIE;
							else if (cmd_args->glblPars.aredDetectedCount != 0)
								cmd_args->glblPars.currQueueType = QUEUETYPE_ARED;
							else
								cmd_args->glblPars.currQueueType = QUEUETYPE_AQM;
						}
						else
							cmd_args->glblPars.currQueueType = QUEUETYPE_TD;
    				}

    				if (cmd_args->glblPars.currQueueType == QUEUETYPE_TD) {
    					cmd_args->glblPars.tada_phase = PACKETB_BYTEB_PHASE;
    					message = htonl(QUEUETYPE_TD);
						if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        					perror("   Send the QueueType message to SND failed!!!");
        					return set_error_signal();
    					}
    				}
    				else {
    					message = htonl(QUEUETYPE_AQM);
						if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        					perror("   Send the QueueType message to SND failed!!!");
        					return set_error_signal();
    					}
    					return 2;    					
    				}
    			}
    			else {
    				message = htonl(QUEUETYPE_AQM);
					if (send(sock, &message, sizeof(message), 0) <= 0 ) {
        				perror("   Send the QueueType message to SND failed!!!");
        				return set_error_signal();
    				}
    			}
			}
		}
		else if (cmd_args->glblPars.tada_phase == RATE_DETECTION_PHASE) {
			calculated_rate = res;
			calculated_rate = htonl(calculated_rate); 
			//2 - Send it back to SND
			if (send(sock, &calculated_rate, sizeof(calculated_rate), 0) <= 0 ) {
        		perror("   Send the Rate message to SND failed!!!");
        		return set_error_signal();
    		}			
			cmd_args->glblPars.tada_phase = AQM_VS_TD_PHASE;
			
		}

        printf("------------------------------------------------------\n\n");
		pthread_mutex_lock(&session_complete_mutex);
			session_complete = 0;
		pthread_mutex_unlock(&session_complete_mutex);

	}

	if (cmd_args->glblPars.test_stage == GETINITIALMESSAGE_STAGE)
		cmd_args->glblPars.test_stage = GETFINISHMESSAGE_STAGE;
	else if (cmd_args->glblPars.test_stage == GETFINISHMESSAGE_STAGE)
		cmd_args->glblPars.test_stage = GETINITIALMESSAGE_STAGE;

	return 1;

	
}