#include <assert.h>

#include "tada.h"

struct cmd_args cmd_args;


double tDistributionTable[TTABLE_ROW_NUMBER][TTABLE_COLUMN_NUMBER];
//double tTestGroup1[DEFAULT_GROUP_SIZE];
//double tTestGroup2[DEFAULT_GROUP_SIZE];

time_t test_time;
int session_complete = 0;
int unexpected_exit = 1;
int suffix = 0;
int udpthread_exit = 0;
int testNumber = 0;

int udp_packetCount = 0;
int udp_packetSize = 0;

int channelError = 0;
int *channelErrorArray;
int channelErrorArraySize = 0;

pthread_mutex_t session_complete_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t unexpected_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t udpthread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t udpthread_exit_cond = PTHREAD_COND_INITIALIZER;


char fileNamePrefix[100] = "empty";       /* prefix of result files names  */
char resultFolderName[100] = "./results";
char queueTimeFileName[500] = "empty";  
char infoFileName[500] = "empty";    
char lossDensityFileName[500] = "empty";      
char lossNumberFileName[500] = "empty";      
char queueDerivationFileName[500] = "empty";
char pctPdtMedianFileName[500] = "empty";
char queueTypeFileName[500] = "empty";

char currentTime[80];
/*
   Sets up a TCP connection for The Control Channel
*/

int initializeControlConn(struct cmd_args *cmd_args){
	int sock;
	struct sockaddr_in server, client;
	int client_port = CONTROLCH_START_PORT;
	int rc,on = 1;

	struct connectionInfo *connInfo = &cmd_args->controlConn;

	/* Initialize rand() */
	srand48((unsigned)time(NULL));

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_other, 0, sizeof(struct sockaddr_in));       /* Clear struct */
	connInfo->si_other.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_other.sin_addr.s_addr = *((unsigned long *) connInfo->host->h_addr_list[0]); /* IP address */
	connInfo->si_other.sin_port = htons(DEFAULT_CONTROLCH_PORT);       /* server port */
		/* Create the socket */
	sock = connInfo->sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0) {
		printf("   Control Channel Connection Failed: %s\n",strerror(errno));
		return ERROR;

	}

	struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}	
	}*/    

	/* Specify outgoing port to avoid reusing ports (which makes analysis more complicated */
	/* Construct the server sockaddr_in structure */
	memset(&client, 0, sizeof(struct sockaddr_in));          /* Clear struct */
	connInfo->si_me.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_me.sin_addr.s_addr = htonl(INADDR_ANY);   /* addr */
	connInfo->si_me.sin_port = htons(DEFAULT_CONTROLCH_PORT);

	int err = -1;
	int portAttempts = 0;
	while ((portAttempts < 100) && (err < 0)) {
		err = bind(sock, (struct sockaddr *) &connInfo->si_me, sizeof(struct sockaddr_in));
		if (err < 0) {

			/* Try to change conn port */
			client_port += 1;
			client.sin_port = htons(client_port);
			portAttempts++;
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 1000000 * 10; // Sleep 10 milliseconds
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		perror("Failed to assign Control Channel port in 100 attempts!!!\n");
		return ERROR;

	}

	assert(err != -1);

	/* Establish connection */
	int connAttempts = 5;
	err = -1;
	while ((connAttempts > 0) && (err < 0)) {
		err = connect(sock, (struct sockaddr *) &connInfo->si_other, sizeof(struct sockaddr_in));
		if (err < 0) {
			connAttempts--;

			/* Try again in 3 seconds */
			struct timespec req;
			req.tv_sec = 3;
			req.tv_nsec = 0;
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		printf("Contorl Channel Failed to connect to server on 5 attempts: %s\n",strerror(errno));
		return ERROR;

	}
	else {
		printf("\n----------------------------------------\n");
		printf("   Control Channel Connected!\n");
		printf("----------------------------------------\n\n");
	}

	assert(err != -1);

	struct pollfd pfd = { .fd = sock, .events = POLLERR };

	nfds_t n = 1;
	if (poll(&pfd, n, 0) < 0) {
		printf("Socket error!\n");
	}

	if (pfd.revents & POLLERR) {
		printf("pipe is broken\n");
	}

	if (!tcp_fd_valid(sock)) {
		printf("Socket NOT valid!\n");
	}

	//TODO: Call The UDP Function to Send the Data

	return 1;
}

int initializeUDPconn(struct cmd_args *cmd_args) {

	int sock;
	int i;

	struct connectionInfo *connInfo = &cmd_args->udpConn;

	/* Initialize rand() */
	srand48((unsigned)time(NULL));

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_other, 0, sizeof(struct sockaddr_in));       /* Clear struct */
	connInfo->si_other.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_other.sin_addr.s_addr = *((unsigned long *) connInfo->host->h_addr_list[0]); /* IP address */
	connInfo->si_other.sin_port = htons(connInfo->server_port);       /* server port */

	/* Create the socket */
	connInfo->sock = sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		printf("Failed to create an UDP socket\n");
		return ERROR;
	}

    struct ifreq interface;

	/*if (cmd_args->interfaceOption == 1) 
	{
		strncpy(interface.ifr_ifrn.ifrn_name, cmd_args->ifname, sizeof(cmd_args->ifname));
    	if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
        	               (char *)&interface, sizeof(interface)) < 0) {
        	perror("setting SO_BINDTODEVICE for UDP");
        	exit(1);
    	}

	}*/

	/* Construct the server sockaddr_in structure */
	memset(&connInfo->si_me, 0, sizeof(struct sockaddr_in));          /* Clear struct */
	connInfo->si_me.sin_family = AF_INET;                  /* Internet/IP */
	connInfo->si_me.sin_addr.s_addr = htonl(INADDR_ANY);   /* addr */

	connInfo->client_port = UDPSTREAM_START_PORT;
	connInfo->si_me.sin_port = htons(connInfo->client_port);

	int err = -1;
	int portAttempts = 0;
	while ((portAttempts < 100) && (err < 0)) {
		err = bind(sock, (struct sockaddr *) &connInfo->si_me, sizeof(struct sockaddr_in));
		if (err < 0) {

			connInfo->client_port+=1;
			connInfo->si_me.sin_port = htons(connInfo->client_port);
			portAttempts++;
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 1000000 * 10; // Sleep 10 milliseconds
			nanosleep(&req, NULL);
		}
	}

	if (err < 0) {
		perror("Failed to assign UDP port in 100 attempts\n");
		return ERROR;
	}

	assert(err != -1);

	return 1;

}

int resetSND(struct cmd_args *cmd_args) {
	struct connectionInfo *udpConn_ci = &cmd_args->udpConn;
	struct connectionInfo *controlConn_ci = &cmd_args->controlConn;
	int rc, on = 1;

	memset(&cmd_args->glblPars, 0, sizeof(struct globalParameters));
	cmd_args->glblPars.currQueueType = -1;

	mkpath(resultFolderName);

	// Set UDP socket to be non-blocking.
	rc = fd_set_blocking(udpConn_ci->sock, 0);

	// Set UDP socket to be non-blocking.
	if (rc < 0)
	{
		perror("Error Setting UDP socket Blocking");
		return ERROR;
	}

	pthread_mutex_lock(&unexpected_exit_mutex);
		unexpected_exit = 0;
	pthread_mutex_unlock(&unexpected_exit_mutex);

	pthread_mutex_lock(&session_complete_mutex);
		session_complete = 0;
	pthread_mutex_unlock(&session_complete_mutex);

	return 1;	

}


int main(int argc, char *argv[]) {

	int status = 1;
	FILE *errorInput;

	memset(&cmd_args, 0, sizeof(struct cmd_args));
	memset(&cmd_args.udpConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.controlConn, 0, sizeof(struct connectionInfo));
	memset(&cmd_args.glblPars, 0, sizeof(struct globalParameters));


	/* Parse command line arguments */
	parse_cmd_args(argc, argv, &cmd_args);

	tDistributionTable[0][0] = 0.0005;
	tDistributionTable[0][1] = 0.001;
	tDistributionTable[0][2] = 0.005;
	tDistributionTable[0][3] = 0.01;
	tDistributionTable[0][4] = 0.02;
	tDistributionTable[0][5] = 0.025;
	tDistributionTable[0][6] = 0.05;
	tDistributionTable[0][7] = 0.1;

	channelError = cmd_args.channelError;

	if(cmd_args.channelError == 1) {
		if ((errorInput = fopen(cmd_args.chennelErrorFile, "r")) <= 0){
    		printf("Could not open ChannelError File!!!\n");
    		printf("Exiting the Program\n");
    		return ERROR;
    	}
    	int number = 0;
    	while (!feof(errorInput)) {
    		channelErrorArraySize++;
    		fscanf(errorInput, "%d\n", &number);
    	}
    	channelErrorArray = calloc(sizeof(int), channelErrorArraySize);

    	fseek(errorInput, SEEK_SET, 0);

    	int i = 0;
    	while (!feof(errorInput)) {
    		fscanf(errorInput, "%d\n", &number);
    		channelErrorArray[i] = number;
    		i++;
    	}

    	fclose(errorInput);

	}
	
	/* Start Control Channel First */
	status = initializeControlConn(&cmd_args);

	/* Start UDP Connection*/
	if (status != -1) {
		//printf("Starting initializeUDPconn()!\n");
		status = initializeUDPconn(&cmd_args);
	}

	if (status != -1) {
		//printf("Starting startSndSideControlCon()!\n");
		status = startSndSideControlCon(&cmd_args);
	}

	
	if (status != 1 && cmd_args.updownOption == 1) {
		printf("Starting DownStream Test!!\n");

		status = resetSND(&cmd_args);

		if (status != -1) {
			startRcvSideControlConn(&cmd_args);
		
			pthread_mutex_lock(&udpthread_exit_mutex);
				while (udpthread_exit == 0)
					pthread_cond_wait(&udpthread_exit_cond, &udpthread_exit_mutex);
			pthread_mutex_unlock(&udpthread_exit_mutex);

			char filename[80];

			FILE *queueTypeFile;
			
			/*Opening Queue Type Result file*/
			strcpy(filename, qType_FILENAME);
			create_resultfile_name(filename, queueTypeFileName, &cmd_args.udpConn.strInfo, 1);

    		if ((queueTypeFile = fopen(queueTypeFileName, "w")) <= 0){
    			printf("Could not Create Queue Type Output file!!!\n");
    			status = -1;
    		}

    		fprintf(queueTypeFile, "%d\n", cmd_args.glblPars.currQueueType);

    		fclose(queueTypeFile);
		}


	}


	printf("\n----------------------------------------\n");
	printf("   Closing Control Connection!\n");
	printf("----------------------------------------\n\n");

	close_program(&cmd_args);

	
}