#include "tada.h"
#include <stdbool.h>
#include <sys/ioctl.h>

double corrWithX(int start, int end, double *x);


void get_pct_pdt_corr(double *data, int start, int end, double *pct, double *pdt, double *corr, int inc) {
	int size = end - start;
	int sortArraySize = ((int)sqrt(size)) + 1;
	int medArraySize = sortArraySize;
	double *sort_array =  calloc(sizeof(double), sortArraySize);
	double *med_array = calloc(sizeof(double), medArraySize);
	int array_index = 0, limit = 0;
	int medArrayIndex = 0, sortArrayIndex = 0;
	int i = 0;
	double sum_pct = 0.0, sum_pdt = 0.0, sum_med = 0.0;

	array_index = start;
	
	while (array_index < end) {
		sortArrayIndex = 0;
		limit = array_index + sortArraySize;
		//printf("From %d to %d\n", array_index,limit);
		if (limit > end)
			limit = end;
		for (;array_index < limit;array_index++) {
			sort_array[sortArrayIndex] = data[array_index];
			sortArrayIndex++;
		}
		sort(sortArrayIndex,sort_array);
		med_array[medArrayIndex] = sort_array[sortArrayIndex/2];
		medArrayIndex++;
	}


	for (i = 1; i < medArrayIndex; i++) {
		if (inc == 1) {
			if ( med_array[i] > med_array[i-1] ) {
      			sum_pct += 1;
    		}			
		}
		else {
			if ( med_array[i] < med_array[i-1] ) {
      			sum_pct += 1;
    		}
		}

    	sum_pdt += fabs(med_array[i] - med_array[i-1]);
  	}

	*pct = sum_pct / (medArrayIndex - 1);
	*pdt = (med_array[medArrayIndex-1] - med_array[0]) / sum_pdt;

	if (medArrayIndex < 10) 
		*corr = 0;
	
	else
		*corr = corrWithX(0, medArrayIndex, med_array);

	free(sort_array);
	free(med_array);
}

double corr(int start, int end, double *x, double *y) {

	int i = 0;
	double xSum = 0.0, ySum = 0.0;
	double x2Sum = 0.0, y2Sum = 0.0, xySum = 0.0; 
	int size = end - start;

	if (size < 100)
		return 0.0;

	for (i = start; i < end; i++) {
		xSum += x[i];
		ySum += y[i];
		x2Sum += (x[i] * x[i]);
		y2Sum += (y[i] * y[i]);
		xySum += (x[i] * y[i]);
	}

	long double correlation = 0.0;
	double first = (size * xySum) - (xSum * ySum);
	double second = sqrt((size * x2Sum) - (xSum * xSum)) * sqrt((size * y2Sum) - (ySum * ySum));
	if (second != 0)
		correlation = first / second;
	else
		correlation = 0.0;

	return correlation;

}

double corrWithX(int start, int end, double *x) {

	int i = 0;
	double index = 0.0;
	double xSum = 0.0, ySum = 0.0;
	double x2Sum = 0.0, y2Sum = 0.0, xySum = 0.0; 
	int size = end - start;

	for (i = start; i < end; i++) {
		xSum += x[i];
		ySum += index;
		x2Sum += (x[i] * x[i]);
		y2Sum += (index * index);
		xySum += (x[i] * index);

		index += 1.0;
	}


	long double correlation = 0.0;
	double first = (size * xySum) - (xSum * ySum);
	double second = sqrt((size * x2Sum) - (xSum * xSum)) * sqrt((size * y2Sum) - (ySum * ySum));
	correlation = first / second;

	return correlation;

}

int find_maximum_index(int size, double *data){
	int i = 1;
	int index = 0;
	double maximum = data[0];
	for (; i < size; i++) {
		if (data[i] > maximum) {
			maximum = data[i];
			index = i;
		}
	}
	return index;
}

double calculate_send_rate(struct streamInfo *stream) {
	int firstRcvPckNr = stream->firstRcvSeqNr;
	int lastRcvPckNr = stream->lastRcvSeqNr;
	int numberOfReceivedPck = stream->pckCountReceived;
	int packetSize = stream->rcvPckSize_byte;
	int i = 0;
	double rate = 0.0;

	double receiveDuration = 0.0;
	
	if (numberOfReceivedPck > 0) {
		receiveDuration = stream->packet[lastRcvPckNr].receiveTime - stream->packet[firstRcvPckNr].receiveTime;

		rate = (double)numberOfReceivedPck * packetSize / receiveDuration;
		printf("\n      Calculated Rate is %f\n" RESET, rate);	
	}
	

	return rate;
}

int calculate_loss_density(struct streamInfo *stream, 
	double *pckLossDensity, double *sm_pckLossDensity, int limit, int maxQtSeqNr) {

    int i,j;
    int seqNrCounter = 0;
    double res = 0;
    double sm_lossDensity, lossDensity;
    int numberOfSentPck = stream->rcvPckCount;
    FILE *output_file;

	char filename[80];
	strcpy(filename, RCV_LOSSDENSITY_FILENAME);
	create_resultfile_name(filename, lossDensityFileName, stream, 0);

    if ((output_file = fopen(lossDensityFileName, "w")) <= 0){
    	printf("   Could not create output file for Loss Density\n");
    	return -1;
    }

    while (seqNrCounter < limit) {
    	lossDensity = 0.0;
    	sm_lossDensity = 0.0;
    	for (j = 0; j < numberOfSentPck; j++) {
    		if (stream->packet[j].payloadSize == 0) {
    			res = -1.0 * ((double)((i - j) * (i - j)) / 2000);
    			lossDensity += ( (double)1.2616 * ((double)exp(res)));
    			if (j < maxQtSeqNr) {
    				if (j == 0) {
    					sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    				}
    				else if (stream->packet[j-1].payloadSize != 0 ||
    					stream->packet[j+1].payloadSize != 0) {
    					sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    				}
    			}
    			else
    				sm_lossDensity += ( (double)1.2616 * ((double)exp(res)));
    			
    		}
    	}
    	if (stream->packet[i].payloadSize != 0) {
    		pckLossDensity[seqNrCounter] = lossDensity;
    		sm_pckLossDensity[seqNrCounter] = sm_lossDensity;
    		seqNrCounter++;
    		fprintf(output_file,"%lf\t",stream->packet[i].rel_sendTime);
    		fprintf(output_file,"%e\n",lossDensity);
    	}
    	i++;
    	
    }

    fclose(output_file);
    return 1;

}


int analyzeResults(struct cmd_args *cmd_args, double lossRate) {

	/* Local Variables for Stream Parameters*/
	printf(KBOLD "\n   Results:\n" RESET);
	printf("\n      lossRate: %d\%\n", (int)lossRate);

	struct streamInfo *stream = &cmd_args->udpConn.strInfo;

	int numberOfReceivedPck = stream->pckCountReceived;
	int numberOfSentPck = stream->rcvPckCount;
	int numberOfLostPck = numberOfSentPck - numberOfReceivedPck;
	int packetSize = stream->rcvPckSize_byte;
	int interTrnsmTime = stream->sentPckITT_micros;
	int transmission_duration = stream->transmission_duration;
	int firstRcvPckNr = stream->firstRcvSeqNr;
	int fastestPckSeqNr = stream->fastestPckSeqNr;
	int firstLostPckSeqNr = -1;

	double calculated_rate = 0.0;
	int i = 0;
	char filename[80];

	FILE *queueTime_output_file, *lossNumber_output_file;
	double max_qTime = 0.0;
	int max_qTimeSeqNr = -1;

	double avg_itt = (double) transmission_duration / numberOfSentPck / 1000000.0; // approximate ITT
	
	/*Opening Queue Time Result file*/
	strcpy(filename, RCV_QUEUETIME_FILENAME);
	create_resultfile_name(filename, queueTimeFileName, &cmd_args->udpConn.strInfo, 0);

    if ((queueTime_output_file = fopen(queueTimeFileName, "w")) <= 0){
    	printf("   Could not create second output file for Packets' Queueing Time\n");
    	return -1.0;
    }

    /* Opening Lost Packet Seq# file */
	strcpy(filename, RCV_LOSSNUMBER_FILENAME);
	create_resultfile_name(filename, lossNumberFileName, &cmd_args->udpConn.strInfo, 0);

    if ((lossNumber_output_file = fopen(lossNumberFileName, "w")) <= 0){
    	printf("   Could not create output file for Loss Packet Number\n");
    	return -1.0;
    }
	/* Open Output file for QueueDelay and LossNumber */

	if (cmd_args->glblPars.tada_phase == RATE_DETECTION_PHASE) {
		calculated_rate = calculate_send_rate(&cmd_args->udpConn.strInfo);
		calculated_rate = ceil(calculated_rate);
		int rate_to_int = (int) calculated_rate;
		

		/* While loop Just For QueueDelay and LossNumber and maybe LossDensity */
		for (i = 0; i < numberOfSentPck; i++) {
			stream->packet[i].rel_sendTime = (double) i * avg_itt;
			if (stream->packet[i].payloadSize != 0) {
				stream->packet[i].queueingDelay = (double) fabs(stream->packet[i].oneWayDelay - stream->min_OWD);
				stream->packet[i].rel_receiveTime = stream->packet[i].receiveTime - stream->packet[firstRcvPckNr].receiveTime;
				//printf("Packet Number %d qdelay is %f\n", i, owd);
				fprintf(queueTime_output_file, "%d\t", i);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			fprintf(queueTime_output_file, "%e\n", stream->packet[i].queueingDelay);

    			if (stream->packet[i].queueingDelay > max_qTime) {
    				max_qTime = stream->packet[i].queueingDelay;
    				max_qTimeSeqNr = i;
    			}
			}
			else {
				if (firstLostPckSeqNr == -1)
    				firstLostPckSeqNr = i;
    			fprintf(lossNumber_output_file, "%d\t", i);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			if ( i < firstRcvPckNr)
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[firstRcvPckNr].queueingDelay);
    			else
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[i-1].queueingDelay);
			}
		}
		


		fclose(queueTime_output_file);
    	fclose(lossNumber_output_file);
		return rate_to_int;
	} // End of if (cmd_args->tada_phase == RATE_DETECTION_PHASE)
	else if (cmd_args->glblPars.tada_phase == AQM_VS_TD_PHASE) {
		/* Parameters */
		int sortArray_size = (int)(sqrt(numberOfReceivedPck)/2);
		int medArray_size = ((int)numberOfReceivedPck / sortArray_size) + 1;
		int medArray_index = 0, sortArray_index = 0;
		int mainArray_index = 0, limit = 0;
		double sum_pct = 0.0, sum_pdt = 0.0;
		double pct = 0.0, pdt = 0.0;
		int increasing_index = 0;
		int nonincreasing_index = 0;
		int pct_index = 0;
		int pdt_index = 0;
		int decreasing_index = 0;
		int increasing_number = 0;
		int lastIncPckSeqNr = 0;
		int max_qTimeIndex = 0;
		int queueType = QUEUETYPE_UNKNOWN;

		/* Allocating Space for sortArray and medArray */
		double *sortArray =  calloc(sizeof(double), sortArray_size);
		double *medArray = calloc(sizeof(double), medArray_size);
		double *minArray = calloc(sizeof(double), medArray_size);

		/* Allocating Space for pckQuDelay and pckSeqNr */
		double *pckQuDelay = calloc(sizeof(double), numberOfReceivedPck);
		int *pckSeqNr = calloc(sizeof(int), numberOfReceivedPck);
		double *pckSendTime = calloc(sizeof(double), numberOfReceivedPck);

		/* While loop */
		/* Calculate QueueDelay and Find Increase Part and Max Queue Delay */

		for (i = 0; i < numberOfSentPck; i++) {
			stream->packet[i].rel_sendTime = (double) i * avg_itt;
			if (stream->packet[i].payloadSize != 0) {
				stream->packet[i].queueingDelay = (double) fabs(stream->packet[i].oneWayDelay - stream->min_OWD);
				stream->packet[i].rel_receiveTime = stream->packet[i].receiveTime - stream->packet[firstRcvPckNr].receiveTime;
				//printf("Packet Number %d qdelay is %f\n", i, owd);
				fprintf(queueTime_output_file, "%d\t", i);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(queueTime_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			fprintf(queueTime_output_file, "%e\n", stream->packet[i].queueingDelay);

    			if (stream->packet[i].queueingDelay > max_qTime) {
    				max_qTime = stream->packet[i].queueingDelay;
    				max_qTimeSeqNr = i;
    				max_qTimeIndex = mainArray_index;
    			}

    			/* Find Median Queue Delay for Current sortArray  */
    			sortArray[sortArray_index] = stream->packet[i].queueingDelay;
    			sortArray_index++;
    			pckQuDelay[mainArray_index] = stream->packet[i].queueingDelay;
    			pckSeqNr[mainArray_index] = i;
    			pckSendTime[mainArray_index] = stream->packet[i].rel_sendTime;
    			mainArray_index++;
    			if (sortArray_index == sortArray_size || mainArray_index >=  numberOfReceivedPck) {
    				sort(sortArray_index,sortArray);
    				medArray[medArray_index] = sortArray[sortArray_index/2];
    				int n = 0;

    				while (sortArray[n] == 0.0)
    					n++;
    				minArray[medArray_index] = sortArray[n];
    				//printf("minArray[%d] is %lf\n", medArray_index, minArray[medArray_index]);
    				medArray_index++;
    				sortArray_index = 0;
    				memset(sortArray, 0.0, sizeof(sortArray));

    			}

			}
			else {
				if (firstLostPckSeqNr == -1)
    				firstLostPckSeqNr = i;
    			fprintf(lossNumber_output_file, "%d\t", i);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_sendTime);
    			fprintf(lossNumber_output_file, "%lf\t", stream->packet[i].rel_receiveTime);
    			if ( i < firstRcvPckNr)
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[firstRcvPckNr].queueingDelay);
    			else
    				fprintf(lossNumber_output_file, "%e\n", stream->packet[i-1].queueingDelay);
			}
		} // End of for (i = 0; i < numberOfSentPck; i++)
		int sw = -1;

		int fastestPckGroupIndex = fastestPckSeqNr / sortArray_size;

		//printf("   Fastest Packet Seq Number: %d\n", fastestPckGroupIndex);

		double firstCorr = 0.0;
		int isFirstIncreasing = 0;

		sw = 0;

		/* Find the Increasing Part Index */
		for (i = fastestPckGroupIndex + 1; i < medArray_index; i++) {
			if ( medArray[i] > medArray[i-1] )
      			sum_pct += 1;
      		sum_pdt += fabs(medArray[i] - medArray[i-1]);

      		pct = sum_pct / (i - fastestPckGroupIndex);

      		pdt = (medArray[i] - medArray[fastestPckGroupIndex]) / sum_pdt;

      		if ((pct > 0.55 && pdt > 0.55)){
				increasing_index = i;
				sw = 1;
      		}
      		if (pct > 0.55)
      			pct_index = i;

      		if (pdt > 0.55)
      			pdt_index = i;

      		if (pct < 0.54 && pdt > (-1 * 0.45) && pdt < 0.45)
      			nonincreasing_index = i;
		}

		printf("   pdt_index: %d, pct_index: %d, increasing_index: %d\n", 
			pdt_index, pct_index, increasing_index);

		if (sw == 1) { // Found an Increasing Part
			

			int fip = increasing_index * sortArray_size;
			pct = 0.0;
			pdt = 0.0;

			get_pct_pdt_corr(pckQuDelay, fastestPckSeqNr, fip, &pct, &pdt, &firstCorr, 1);

			printf("   PCT and PDT for FIP: %lf, %lf\n", pct, pdt);

			if (pct > 0.5 && pdt > 0.4) {
				isFirstIncreasing = 1;
				pct = 0.0;
				pdt = 0.0;
				firstCorr = 0.0;
			}

			if (isFirstIncreasing == 1) {
				increasing_number = (increasing_index + 1) * sortArray_size;
			}
			else {
				increasing_number = 0;
				if (pct < 0.54 && pdt > (-1 * 0.45) && pdt < 0.5)
					queueType = QUEUETYPE_TD;
				else if (pct < 0.54 && pdt > 0.55)  {
					increasing_number = (increasing_index + 1) * sortArray_size;
				}
			}
		}
	
		 
		if (increasing_number > numberOfReceivedPck)
			increasing_number = numberOfReceivedPck;

		


		double ratio = ((double) increasing_number / numberOfReceivedPck);
		
		//printf("   Increasing Number: %d\n", increasing_number);
		
		if ((ratio > 0.025)) {

			if (increasing_number != numberOfReceivedPck)
				increasing_number = (increasing_index + 5) * sortArray_size;

			if (increasing_number > numberOfReceivedPck)
				increasing_number = numberOfReceivedPck;

			lastIncPckSeqNr = pckSeqNr[increasing_number - 1];
			/* Find out if MaxQ is in the range */
			if (max_qTimeSeqNr > lastIncPckSeqNr) {
				max_qTimeIndex = find_maximum_index(increasing_number, pckQuDelay);
				max_qTimeSeqNr = pckSeqNr[max_qTimeIndex];
			}

			if (firstLostPckSeqNr != -1 && 
				max_qTimeSeqNr <= firstLostPckSeqNr &&
				lossRate > 20.0) {				
				queueType = QUEUETYPE_TD;
				cmd_args->glblPars.isDefintlyTD = 1;
				
			}
			else if (firstLostPckSeqNr == -1) {
				if (pckQuDelay[max_qTimeSeqNr] > 0.15) {
					queueType = QUEUETYPE_TD;
					cmd_args->glblPars.isDefintlyTD = 1;
				
				}
			}
			else if (lossRate > 15.0) {

				queueType = QUEUETYPE_AQM;

				double *pckLossDensity = calloc(sizeof(double), increasing_number);
				double *sm_pckLossDensity = calloc(sizeof(double), increasing_number);
				calculate_loss_density(&cmd_args->udpConn.strInfo, pckLossDensity, 
					sm_pckLossDensity, increasing_number, max_qTimeSeqNr);					

				int end_index = 0;
				int limit = increasing_number * 0.95;

				if (ratio > 0.95 && max_qTimeIndex > limit)
					end_index = max_qTimeIndex * 0.95;
				else
					end_index = max_qTimeIndex;

				double total_pct = 0.0, total_pdt = 0.0;
				int isLeftPartIncreasing = 0, isRightPartIncreasing = 0;
				int isTotalIncreasing = 0;
				double total_corr = 0.0;
				double leftPartCorr = 0.0; // Left Part 
				double rightPartCorr = 0.0;

				/* Divide the whole priod to two section and calculate the correlation */

				double leftPartPct = 0.0, leftPartPdt = 0.0;
				double rightPartPct = 0.0, rightPartPdt = 0.0;
				int sw = 0; // A switch set to 1 when one of the parts has low correlation

				int middleIndex = (end_index - firstLostPckSeqNr) * 0.5 + firstLostPckSeqNr;
				//printf("   MaxQtimeIndex: %d\n", max_qTimeIndex);
				//printf("   First Loss SeqNr: %d\n", firstLostPckSeqNr);
				//printf("   Middle Index: %d\n", middleIndex);
				//printf("   MaxQtimeIndex * 0.75: %d\n", end_index);
				//printf("   Loss Density Total Range CORR %lf\n", total_corr);

				pct = 0.0, pdt = 0.0;
				double tempCorr = 0.0;

				total_corr = corr(firstLostPckSeqNr, end_index, pckSendTime, sm_pckLossDensity);
				leftPartCorr = corr(firstLostPckSeqNr, middleIndex, pckSendTime, sm_pckLossDensity);
				rightPartCorr = corr(middleIndex, end_index, pckSendTime, sm_pckLossDensity);
				get_pct_pdt_corr(pckLossDensity, firstLostPckSeqNr, end_index, &pct, &pdt, &tempCorr, 1);

				printf("   TotalCorr is: %lf\n", total_corr);
				printf("   rightPartCorr is: %lf\n", rightPartCorr);
				printf("   leftPartCorr is: %lf\n", leftPartCorr);
				printf("   LossD PCT is %lf\n", pct);
				printf("   LossD PDT is %lf\n", pdt);
				
				if (total_corr <= DEFAULT_CORR_THRESHOLD) {
					queueType = QUEUETYPE_TD;
				}
				else if (leftPartCorr < 0.7) {
					int start = 0, end = start + 3 * sortArray_size;
					double slope = 0.0, angle = 0.0;
					double val = 180.0 / PI;
					do {
						
						slope = 0.0;
						angle = 0.0;

						slope_calculator(pckSeqNr, pckQuDelay, start, end, 
							minArray[0], &slope);

						angle = (double)atan(slope) * val;
						if (fabs(angle) <= 15.00) {
							double sm_rangeCorr = corr(start, end_index, pckSendTime, sm_pckLossDensity);
							if (sm_rangeCorr <= DEFAULT_CORR_THRESHOLD) {
								double m_rangeCorr = corr(start, end_index, pckSendTime, pckLossDensity);
								if (m_rangeCorr <= DEFAULT_CORR_THRESHOLD) {
									printf("   rangeCorr from %d to %d is %lf\n", 
										start, end_index, m_rangeCorr);
									queueType = QUEUETYPE_TD;
									break;
								}
								 
							}
						}
							
						start += 1;
						end += 1;

					} while (end <= end_index);

				}
				
				int maxQtGroupIndex = max_qTimeIndex / sortArray_size;
										
				if (queueType != QUEUETYPE_TD &&
					(medArray_index - maxQtGroupIndex - 1) >= 10) {


					//queueType = QUEUETYPE_PIE;

					//double slope = 0.0, angle = 0.0;
					//double val = 180.0 / PI;
						

					//slope_calculator(pckSeqNr, pckQuDelay, max_qTimeSeqNr, numberOfReceivedPck, 
					//		minArray[0], &slope);

					//angle = (double)atan(slope) * val;
					//if (fabs(angle) <= 10.00)
						//queueType = QUEUETYPE_ARED;

					queueType = QUEUETYPE_ARED;
					
					sum_pct = 0.0, sum_pdt = 0.0;
					sw = 0;
					decreasing_index = 0;
					for (i = maxQtGroupIndex + 1; i < medArray_index; i++) {
						if ( medArray[i] < medArray[i-1] )
      						sum_pct += 1;
      					sum_pdt += fabs(medArray[i] - medArray[i-1]);

      					pct = sum_pct / (i - maxQtGroupIndex - 1);

      					pdt = (medArray[i] - medArray[maxQtGroupIndex+1]) / sum_pdt;

      					if ((pct > 0.55 && pdt < (-1 * 0.55))){
							decreasing_index = i;
							sw = 1;
      					}
					}

					double decRatio = 0.0;
					double incRatio = 0.0;

					if (sw == 1) {
						int decNum = (decreasing_index + 1) * sortArray_size;
						if (decNum > numberOfReceivedPck)
							decNum = numberOfReceivedPck;
						double max = 0.0;
						double min = 0.0;

						decRatio = ((double) (decreasing_index - maxQtGroupIndex) * sortArray_size / numberOfReceivedPck);
						incRatio = (double)max_qTimeIndex / numberOfReceivedPck;

						if (decreasing_index == medArray_index -1)
							queueType = QUEUETYPE_PIE;
						else if (decRatio >= (incRatio)) {
							
							find_min(pckQuDelay, max_qTimeSeqNr, decNum, &min);

							printf("Minimum from %d to %d is %lf\n", max_qTimeSeqNr, decNum, min);
							 
							if (min == 0.0)
								queueType = QUEUETYPE_PIE;
							else {
								double maxTomin = (double) pckQuDelay[max_qTimeSeqNr] / min;
								if (maxTomin >= 8.0)
									queueType = QUEUETYPE_PIE;
							} 
								
						}
							
					}


					if (queueType == QUEUETYPE_PIE){
						int arrayEnd = max_qTimeIndex;
						arrayEnd = arrayEnd * 0.85;
						queueType == QUEUETYPE_PIE;
						//if ((arrayEnd - firstLostPckSeqNr) > 9 * DEFAULT_GROUP_SIZE) 
						{
							int arrayStart = firstLostPckSeqNr;
							
							double incPercentage = 0.0;
							int incRound = 0;
							int currGroupSize = 0;
							int roundNumber = 1;

							cmd_args->glblPars.rSquaredCount++;


							double R_Squared = corr(arrayStart, arrayEnd, pckSendTime, sm_pckLossDensity);
							double avgRsquared = 0.0;

							R_Squared *= R_Squared;

							cmd_args->glblPars.currRsquared += R_Squared;

							avgRsquared = cmd_args->glblPars.currRsquared / (double)cmd_args->glblPars.rSquaredCount;

							if (R_Squared >= 0.9)
								queueType = QUEUETYPE_CODEL;

							/*group1Size = find_group_size(arrayStart, arrayEnd);
							calculate_standardDeviation(pckLossDensity, arrayStart, group1Size, &sd1, &avg1);
							arrayStart += group1Size;
							
							do {
								group2Size = find_group_size(arrayStart, arrayEnd);
								calculate_standardDeviation(pckLossDensity, arrayStart, group2Size, &sd2, &avg2);
								calculate_standardError(group1Size, group2Size, sd1, sd2, &se);
								calculate_pValue(group1Size, group2Size, avg1, avg2, se, &pValue);

								calculate_confidenceInterval(group1Size, group2Size, avg1, avg2, se,
									&upperB, &lowerB);


								if (pValue < 0.05)
									incRound++;

								roundNumber++;
								group1Size = group2Size;
								sd1 = sd2;
								avg1 = avg2;
								arrayStart += group1Size;
							} while (arrayStart < arrayEnd);
								
							if (roundNumber > 0)
								incPercentage = (double)incRound/roundNumber;*/
						}
					}
				}

				free(pckLossDensity);
				free(sm_pckLossDensity);	
			} 

		}
		

		free(sortArray);
		free(medArray);
		free(minArray);
		free(pckQuDelay);
		free(pckSendTime);
		free(pckSeqNr);
		fclose(queueTime_output_file);
    	fclose(lossNumber_output_file);
    	return queueType;

	} // End of if (cmd_args->tada_phase == AQM_VS_TD_PHASE)
	
	return 0;

	
	/* Close Output file for QueueDelay and LossNumber */

}