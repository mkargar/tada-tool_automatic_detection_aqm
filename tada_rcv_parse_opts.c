#include "tada.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

#define OPTSTRING "u:c:I:P:o:i:p:m:d:Trah"

void usage(char* argv) {

	printf("\nUsage: %s [-u] [-I] [-a] [-P] [-o] [-r] [-c] [-T] [-I] [-i] [-p] [-m] [-d] \n", argv);
	printf("Required options:\n");
	printf("  -u <UDP port>                  : UDP Port to listen to.\n\n");

	printf("Upstream options:\n");
	printf("  -a                             : Automatic test\n");
	printf("  -P <namePrefixStream>          : Name Prefix for Result Files: Default is empty\n");
	printf("  -o <outputFolderName>          : Result Folder Name: Default is ./result\n");
	printf("  -r                             : Keep Packets Receive Time\n\n");
	printf("  -c <ControlChannelPortNumber>  : Control port to connect to (Default 8181) \n\n");
	
	printf("Downstream options:\n");
	printf(" -T                             : Up/Down Stream Queue Detection\n");
	printf(" -i <InterTransmissionTime>     : Packets ITT (non-auto test), in micro seconds.(Dfeault 0)\n");
	printf(" -p <PacketNumber>              : Number of UDP Stream Packet (auto and non-auto). (Default 2000)\n");
	printf(" -m <PacketSize>                : Packet Size (auto and non-auto) in Byte. (Default 1472B)\n");
	printf(" -d <testDuration>              : Test Duration (auto and non-auto) in Seconds. (Default 7)\n\n");


	exit(1);
}

void parse_cmd_args(int argc, char *argv[], struct cmd_args *cmd_args) {


	FILE *tTableFile;
	char comma;
	int i, j;
	char str[50];


    if ((tTableFile = fopen("./tDistributionTable/tTable.txt", "r")) <= 0){
    	printf("Error: Could not open The T-Distribution Table File\n");
    	exit(1);
    }

	for (i = 1; i < TTABLE_ROW_NUMBER; i++)
    	for (j = 0; j < TTABLE_COLUMN_NUMBER; j++) 
    		tDistributionTable[i][j] = 0.0;

    for (i = 1; i < TTABLE_ROW_NUMBER; i++)
    	for (j = 0; j < TTABLE_COLUMN_NUMBER; j++) {
    		if (fscanf(tTableFile, "%s\t", str) <= 0) {
    			printf("Error Reading From tTable File\n");
    			exit(1);
    		}
    		else {
    			sscanf(str, "%lf", &tDistributionTable[i][j]);
    			//printf("tTable Value[%d][%d] is %f\n", i, j, tDistributionTable[i][j]);
    		}
    	}
	
	
    fclose(tTableFile);

	int c;	
	int arg_off;
	cmd_args->udpConn.dev_null = NULL;
	cmd_args->controlConn.dev_null = NULL;
	cmd_args->udpConn.strInfo.packet = NULL;
	cmd_args->controlConn.strInfo.packet = NULL;
	
	while (1) {
		c = getopt(argc, argv, OPTSTRING);
		if(c == -1) break;

		switch(c){

		case 0:
			break;

		case 'u':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.server_port = atoi(optarg);
			arg_off += 2;
			break;

		//TODO: Add "-I" option for interface

		case 'c':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->controlConn.server_port = atoi(optarg);
			arg_off += 2;
			break;

		case 'I' :
			required_argument_check(optarg, cmd_args, c);
			cmd_args->ifname = optarg;
			cmd_args->interfaceOption = 1;
			break;

		case 'P':
			required_argument_check(optarg, cmd_args, c);
			strcpy(fileNamePrefix, optarg);
			arg_off += 2;
			break;

		case 'r' :
			cmd_args->pckReceiveTime = 1;
			arg_off++;
			break;

		case 'a' :
			cmd_args->is_automatic_test = 1;
			arg_off++;
			break;

		case 'o':
			required_argument_check(optarg, cmd_args, c);
			strcpy(resultFolderName, optarg);
			arg_off += 2;
			break;

		case 'T' :
			cmd_args->updownOption = 1;
			arg_off++;
			break;

		case 'i': //intertransmissiontime
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckITT_micros = next_int(&optarg);
			break;

		case 'p': //packet number
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckCount = next_int(&optarg);
			break;

		case 'm':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->udpConn.strInfo.sentPckSize_byte = next_int(&optarg);
			break;

		case 'd':
			required_argument_check(optarg, cmd_args, c);
			cmd_args->sndSideTransmission_duration = next_int(&optarg);
			break;	

		case 'h' :
			usage(argv[0]);
			arg_off++;
			break;

		case '?' :
			usage(argv[0]);
			arg_off++;
			break;

		case ':' :
			usage(argv[0]);
			arg_off++;
			break;

		default:
			break;
		}
	}

	/* make the result folder */

	
	if ( !cmd_args->udpConn.server_port) {
		printf("ERROR: you must specify UDP port!\n\n");
		usage(argv[0]);
		exit(1);
	}

	if (!cmd_args->controlConn.server_port)
		cmd_args->controlConn.server_port = DEFAULT_CONTROLCH_PORT;

	if (cmd_args->updownOption == 1) {
		if (cmd_args->udpConn.strInfo.sentPckSize_byte == 0) {
			cmd_args->udpConn.strInfo.sentPckSize_byte = MAX_PACKETSIZE_DEFAULT;
		}

		if (cmd_args->udpConn.strInfo.sentPckCount == 0 && 
			cmd_args->sndSideTransmission_duration == 0) {
			cmd_args->udpConn.strInfo.transmission_duration = 0;
			cmd_args->udpConn.strInfo.sentPckCount = DEFAULT_PACKET_COUNT;
		}
		else if (cmd_args->udpConn.strInfo.sentPckCount != 0 && 
			cmd_args->udpConn.strInfo.transmission_duration != 0) {
			printf("Error: Can not specify both the number of packets and the test Duration!\n");
			usage(argv[0]);
			exit(1);
		}

	}


	
    mkpath(resultFolderName);



}